//package root.user;
//
//import static org.hamcrest.Matchers.*;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//import static io.restassured.RestAssured.given;
//
//import io.restassured.RestAssured;
//import io.restassured.http.ContentType;
//import root.security.JwtResponseToken;
//import root.security.SignInRequest;
//import root.user.doctor.DoctorDTO;
//import root.user.patient.PatientDTO;
//
//public class PatientUserTest {
//	
//	
//	public  PatientUserTest() {
////		RestAssured.baseURI="http://localhost";
//		
//		RestAssured.baseURI="http://ec2-52-91-212-167.compute-1.amazonaws.com";
//		RestAssured.port = 8080;
//	}
//	@Test
//	public void testCreatePatient()
//	{
//	  PatientDTO bodyPatientDTO = new PatientDTO("Testing Name","fortify",150,"giza", "bardo zy al 3ada mafesh","+201245670");
//	  given()
//			.contentType(ContentType.JSON)
//			.body(bodyPatientDTO)
//		.when()
//			.post("/patient/api/createPatient")
//		.then()
//		.log()
//		.all();
//		
//	}
//	@Test
//	public void testSignIn()
//	{
//
//	  SignInRequest signInUser= new SignInRequest("+201245670","fortify");
//	
//	  JwtResponseToken resp  = given()
//			.contentType(ContentType.JSON)
//			.body(signInUser)
//		.when()
//			.post("/signIn")
//			.then()
//			 .extract()
//			 .as(JwtResponseToken.class);
//	  Assertions.assertThat(resp.getToken()).isNotNull();
//
//	}
//	@Test
//	public void testGetPatientData()
//	{
//	  given()
//			.header("Content-Type","application/json")
//			.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1NjcwIiwiY3JlYXRlZCI6MTU4MzU5NjY3OTg1MCwiZXhwIjoxNTg1MzI0Njc5fQ.-gT7ZUs618FBpV_bPdnlNCH77HNRWr3qflQ2dzFlOL7WIAVgxNeBVhiq9ZgCe4wCUtGkCQTIpqrSLJuH1pQo5Q")
//			.contentType(ContentType.JSON)
//		.when()
//			.get("/patient/getCurrentPatientData")
//		.then()
//		.statusCode(200)
//		.body("id", equalTo(60));
//
//	}
//	@Test
//	public void testUpdatePatientData()
//	{
//	PatientDTO bodyPatientDTO = new PatientDTO("Testing Changed Name",50,"giza changed");
//	  given()
//			.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1NjcwIiwiY3JlYXRlZCI6MTU4MzU5NjY3OTg1MCwiZXhwIjoxNTg1MzI0Njc5fQ.-gT7ZUs618FBpV_bPdnlNCH77HNRWr3qflQ2dzFlOL7WIAVgxNeBVhiq9ZgCe4wCUtGkCQTIpqrSLJuH1pQo5Q")
//			.contentType(ContentType.JSON)
//			.body(bodyPatientDTO)
//		.when()
//			.post("/patient/updateProfile")
//		.then()
//		.statusCode(200)
//		.body("age", equalTo(50),
//				"address",equalTo("giza changed"));
//
//	}
//	@Test
//	public void testGetDoctorz()
//	{
//	DoctorDTO bodyDTO = new DoctorDTO("change Doc Name",30);
//	  given()
//		.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1NjcwIiwiY3JlYXRlZCI6MTU4MzU5NjY3OTg1MCwiZXhwIjoxNTg1MzI0Njc5fQ.-gT7ZUs618FBpV_bPdnlNCH77HNRWr3qflQ2dzFlOL7WIAVgxNeBVhiq9ZgCe4wCUtGkCQTIpqrSLJuH1pQo5Q")
//			.contentType(ContentType.JSON)
//			.body(bodyDTO)
//		.when()
//			.post("/DoctorPatient/getMyDoctorList")
//		.then()
//		.log()
//		.all();
//
//	}
//}

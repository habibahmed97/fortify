//package root.user;
//
//import static org.hamcrest.Matchers.*;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.assertj.core.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//import static io.restassured.RestAssured.given;
//
//import io.restassured.RestAssured;
//import io.restassured.http.ContentType;
//import root.security.JwtResponseToken;
//import root.security.SignInRequest;
//import root.user.doctor.DoctorDTO;
//
//public class DoctorUserTest {
//	
//	public  DoctorUserTest() {
////		RestAssured.baseURI="http://localhost";
//		RestAssured.baseURI="http://ec2-52-91-212-167.compute-1.amazonaws.com";
//
//		RestAssured.port = 8080;
//	}
//
//		
//	
//	@Test
//	public void testCreateDoctor()
//	{
//		DoctorDTO bodyDTO = new DoctorDTO("Testing Doctor Name","fortify",30,"fcm Token Not Yet", "+2012456780");
//	  given()
//			.contentType(ContentType.JSON)
//			.body(bodyDTO)
//		.when()
//			.post("/doctor/api/createDoctor")
//		.then()
//		.log()
//		.all();
//		
//	}
//	@Test
//	public void testSignIn()
//	{
//
//	  SignInRequest signInUser= new SignInRequest("+2012456780","fortify");
//	
//	  JwtResponseToken resp  = given()
//			.contentType(ContentType.JSON)
//			.body(signInUser)
//		.when()
//			.post("/signIn")
//			.then()
//			 .extract()
//			 .as(JwtResponseToken.class);
//	  Assertions.assertThat(resp.getToken()).isNotNull();
//
//	}
//	@Test
//	public void testGetDoctorData()
//	{
//	  given()
//			.header("Content-Type","application/json")
//			.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1Njc4MCIsImNyZWF0ZWQiOjE1ODM2MTIxNTI3NjksImV4cCI6MTU4NTM0MDE1Mn0.VrDPHs160fi-Fh1qXLLgsS_32-P_Mz-hVZZIbyxBtrSwOc8BCd0RQziIqgvyz_AKk-BiJT-WErF9uweCzhniOg")
//			.contentType(ContentType.JSON)
//		.when()
//			.get("/doctor/getCurrentDoctorData")
//		.then()
//		.statusCode(200)
//		.body("id", equalTo(61));
//
//	}
//	@Test
//	public void testUpdateDoctorData()
//	{
//	DoctorDTO bodyDTO = new DoctorDTO("Changed Doc Name",30);
//	  given()
//		.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1Njc4MCIsImNyZWF0ZWQiOjE1ODM2MTIxNTI3NjksImV4cCI6MTU4NTM0MDE1Mn0.VrDPHs160fi-Fh1qXLLgsS_32-P_Mz-hVZZIbyxBtrSwOc8BCd0RQziIqgvyz_AKk-BiJT-WErF9uweCzhniOg")
//			.contentType(ContentType.JSON)
//			.body(bodyDTO)
//		.when()
//			.post("/doctor/updateProfile")
//		.then()
//		.statusCode(200)
//		.body("id", equalTo(61),
//				"years_experience",equalTo(30));
//
//	}
//	@Test
//	public void testAddPatient()
//	{
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("mobile", "+201245670");	 
//		given()
//		.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1Njc4MCIsImNyZWF0ZWQiOjE1ODM2MTIxNTI3NjksImV4cCI6MTU4NTM0MDE1Mn0.VrDPHs160fi-Fh1qXLLgsS_32-P_Mz-hVZZIbyxBtrSwOc8BCd0RQziIqgvyz_AKk-BiJT-WErF9uweCzhniOg")
//		.contentType(ContentType.JSON)
//		.body(map)
//		.when()
//			.post("/addPatient")
//			.then()
//			.statusCode(200)
//			.body("id", equalTo(60));
//		
//	}
//	
//	@Test
//	public void testGetPatients()
//	{
//	  given()
//			.header("Content-Type","application/json")
//			.header("Authorization", "Bearer "+"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIrMjAxMjQ1Njc4MCIsImNyZWF0ZWQiOjE1ODM2MTIxNTI3NjksImV4cCI6MTU4NTM0MDE1Mn0.VrDPHs160fi-Fh1qXLLgsS_32-P_Mz-hVZZIbyxBtrSwOc8BCd0RQziIqgvyz_AKk-BiJT-WErF9uweCzhniOg")
//			.contentType(ContentType.JSON)
//		.when()
//			.get("/DoctorPatient/getMyPatientList")
//			.then()
//			.log()
//			.all();
//
//	}
//}

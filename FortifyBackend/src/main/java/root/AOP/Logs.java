package root.AOP;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Aspect
@Component
public class Logs {

	//static method and takes class that do actual logging
	Logger logger = LoggerFactory.getLogger(Logs.class);
	
	@Before("execution(* root.user.patient.PatientController.*(..)) ||"
			+ " execution(* root.user.doctor.DoctorController.*(..)) ||"
			+ " execution(* root.security.SignInAuthController.*(..))" )
	public void logsBeforePatientController(JoinPoint joinPoint) {
	  	  logger.trace("Fn Name: "+ joinPoint.getSignature().getName());
	  	  logger.trace("BEFORE: 	" +Arrays.toString(joinPoint.getArgs()));
	  
	
	}	
	
	@AfterReturning(pointcut = "execution(* root.user.patient.PatientController.*(..)) ||"
			+ " execution(* root.user.doctor.DoctorController.*(..)) ||"
			+ " execution(* root.security.SignInAuthController.*(..))"
			,returning="result")
	public void logsAfterPatientController(JoinPoint joinPoint, Object result) {
	  logger.trace("AFTER:	 "+ result.toString());
	}
	

}

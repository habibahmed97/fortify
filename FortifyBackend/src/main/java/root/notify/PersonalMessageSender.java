package root.notify;


import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import root.error.ConflictException;

@Service
public class PersonalMessageSender {
	
	
	private final static String FIREBASE_API_URL = "https://fcm.googleapis.com/fcm/send";
	private final static String FIREBASE_SERVER_KEY = "AAAArKDEovk:APA91bEbcO4iFqEdJ77lwOqekchg67kEopZBXHCcG5tcfRTGcAjvyN506oflj1GMqjM-Q8xIAkCLO0zKRsSN3V_vvwIT2IiXNi21m0qgmSCCK_-Tv6PfBfUrTJRV91289oEdJ6YxLVVg";
	    
	public static String callToFcmServer(notification notification, data data, String receiverFcmKey) throws Exception 
	{
	        
	   RestTemplate restTemplate = new RestTemplate();
	   HttpHeaders httpHeaders = new HttpHeaders();
	   httpHeaders.set("Authorization", "key=" + FIREBASE_SERVER_KEY);
	   httpHeaders.set("Content-Type", "application/json");
	        
	   JSONObject json = new JSONObject();
	   try {
		   		//creating New Json object from notification classs string coverted to JSON
		        json.put("data",  new JSONObject(new Gson().toJson(data)));
		        json.put("notification", (new JSONObject((new Gson().toJson(notification))).put( "content-available","0")).put("click_action","FCM_PLUGIN_ACTIVITY"));		        json.put("to", receiverFcmKey);
		        HttpEntity<String> httpEntity = new HttpEntity<>(json.toString(), httpHeaders);
		        return restTemplate.postForObject(FIREBASE_API_URL, httpEntity, String.class);
		 }
		 catch (JSONException e) {
			  throw new ConflictException(String.format("Error :[%s] ", e.getMessage().toString()));
			  
		 }
	       

	        
   }
	    

  }



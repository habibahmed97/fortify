package root.notify;

public class data {
	 String notificationType;
	 String  title;
	 String body;
	 int notId;
	 int badge ;
		
	public data() {
		this.title = "This is data Title";
		this.body = "This is data Body";
		this.notificationType="Test";
		this.notId=10;
		this.badge=7;
	}
	public data(String title, String body)
	{
		this();
		this.title = title;
		this.body = body;
		
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
}

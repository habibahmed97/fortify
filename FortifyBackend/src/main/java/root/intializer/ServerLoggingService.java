package root.intializer;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServerLoggingService {
	 @Autowired
	  private ServletContext servletContext;

	  @PostConstruct
	  public void printServer() {
	      System.out.println("Server Version: " + this.servletContext.getServerInfo());
	  }
}

package root.intializer;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;




@Component
public class FirstTimeInitializer implements CommandLineRunner {

    private final Log logger = LogFactory.getLog(FirstTimeInitializer.class);


    @Override
    public void run(String... strings) throws Exception {
    	//Write whatever you want which will be executed only one time when serve is up and running
    	logger.info("Habiba: App is On and Running");

    	
    	
    }
}

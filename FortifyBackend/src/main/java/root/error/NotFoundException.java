package root.error;

import org.springframework.http.HttpStatus;


public class NotFoundException extends APIExceptionBase{
	

	private static final long serialVersionUID = 1L;

	public NotFoundException(String message) {
	super(message);
	}
	
	@Override
	public HttpStatus getStatusCode()
	{
		return HttpStatus.NOT_FOUND;
	}
	
}
package root.error;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ValidationError {
	
	private List<String> errors;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private Date timestamp;
	
	public ValidationError() {
        this.timestamp = new Date();
        this.errors = new ArrayList<>();
    }
	public String uri;
	
	public List<String> getErrors() {
		return errors;
	}
	
	public void addError(String error) {
        this.errors.add(error);
    }

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}


	
 
}

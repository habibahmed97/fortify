package root.error;

import org.springframework.http.HttpStatus;

public class ConflictException extends APIExceptionBase{

	
	private static final long serialVersionUID = 1L;

	public ConflictException(String message) {
		super(message);
	}
		
	@Override
	public HttpStatus getStatusCode()
	{
			return HttpStatus.CONFLICT;
	}
		
}

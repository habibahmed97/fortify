package root.error;


import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler{


	 @ExceptionHandler(Exception.class)
	 public ResponseEntity<ErrorDetails> handleApiException(APIExceptionBase ex, WebRequest request)
	 {
		 ErrorDetails details = new ErrorDetails(ex.getMessage(), request.getDescription(false));
		 return new ResponseEntity<>(details, ex.getStatusCode());
	 }
	 
	 //Validation of each parameter
	  @Override //over riding method in response entity exception handler class
	    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		  	ValidationError validationerror = new ValidationError();
	        validationerror.uri = request.getDescription(false);
	        List<FieldError> errors = ex.getBindingResult().getFieldErrors();
	        
	        for(FieldError error:errors)
	        {
	        	validationerror.addError(error.getDefaultMessage());
	        }
	        return new ResponseEntity<>(validationerror,HttpStatus.BAD_REQUEST);
	    }
}

package root.error;

import org.springframework.http.HttpStatus;

public abstract class APIExceptionBase extends RuntimeException {


	private static final long serialVersionUID = 1L;


	public APIExceptionBase(String message) {
		super(message);
	}
	
	
	public abstract HttpStatus getStatusCode();
	
	
	
}

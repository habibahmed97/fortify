package root.security;

public class JwtResponseToken {
	 
	String token;
	
	public JwtResponseToken() {
	}
	
	public JwtResponseToken(String token) {
	 setToken(token);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	

}

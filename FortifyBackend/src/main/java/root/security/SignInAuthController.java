package root.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import root.error.ConflictException;


@RestController
@RequestMapping(value = "/signIn")
public class SignInAuthController extends BaseController {

	    @Autowired
	    private TokenUtilities tokenUtil;

	  
	    @Autowired
	    private AuthenticationManager authenticationManager;
	    
	  

		@CrossOrigin
	    @PostMapping(value = {"","/"})
	    public JwtResponseToken signIn(@RequestBody SignInRequest signInRequest) {
		

			//ERROR AUTHENTICATION SELECT USRE WITH LOAD BY USERNAME
	        final Authentication authentication = authenticationManager.authenticate(
	                new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword())
	        );
	        SecurityContextHolder.getContext().setAuthentication(authentication);
	        
	        try {
		        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		        String token = tokenUtil.generateToken(userDetails);
		        JwtResponseToken response = new JwtResponseToken(token);
		        return response;

	        }
	        catch (Exception e) {
	        	throw new ConflictException("This User Doesn't Exist: "+ e.getMessage().toString());
	        }
	       

			

	    }
		
		

}

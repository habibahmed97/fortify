package root.user.patient;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import root.user.User;


@Table(name = "patient") //This is required if your table name is different from the class name.
@Entity 
public class patient implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String USER_TYPE ="patient";

	@Id
	private int id;
	
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "id")
	private User user;
	
	
	@NotNull(message = "Age CAN'T BE EMPTY")
	@Column(name = "age")
	private int age;
	
//	@OneToMany(cascade = CascadeType.ALL)
//	@JoinColumn(name="patientid")
//    private List<PatienDoctorRelation> relationToPatient;
	
	private String address;
	 

	public patient() {}
	
	public patient(int id,int age)
	{
		this.setId(id);
		this.setAge(age);
	}
	
	public patient(int id, int age,String address)
	{
		this(id,age);
		this.setAddress(address);
	}
	
	public patient(User user)
	{
		this(user.getId(),0);
		this.setAddress("");
		this.setUser(user);
	}
	
//	public List<PatienDoctorRelation> getRelationToPatient() {
//		return relationToPatient;
//	}
//
//
//	public void add(PatienDoctorRelation relation)
//	{
//		if(relationToPatient==null)
//		{
//			relationToPatient = new ArrayList<PatienDoctorRelation>();
//		}
//		relationToPatient.add(relation);
//		
//	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	   
}

package root.user.patient;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import org.springframework.security.authentication.AuthenticationManager;


import root.error.ConflictException;
import root.notify.PersonalMessageSender;
import root.notify.data;
import root.notify.notification;
import root.s3.AmazonClient;
import root.s3.url;
import root.user.User;
import root.user.UserService;

@Service
public class PatientService {
	
		@Autowired
		private UserService userService;

		@Autowired
		private PatientRepository patientRepository;
		
		    
	
		@Autowired
	    private AuthenticationManager authenticationManager;
		
		public patientReturned getPatientByUserId(int id) 
		{

			patient patient = patientRepository.findById(id).get();
			
			if(patient==null) 
			{
				throw new ConflictException(String.format("You are not a Patient Record with Id [%s] in our database", id));
			}
		
			return new patientReturned(patient,patientRepository.getDoctorList(patient.getId()) );
					    
	    }
		
		
		

			
		public patient createPatient(PatientDTO patientModel)
		{	
			User user = userService.SendIfUserExist(patientModel.getMobile());
			
			
			
			if(user!=null) //Exist
			{
			     
			        try {
			        	
						final Authentication authentication = authenticationManager.authenticate(
					                new UsernamePasswordAuthenticationToken(patientModel.getMobile(), patientModel.getPassword())
					        );   
			        	patientRepository.addNewestVitals(user.getId());

			        	UpdatefcmToken(patientModel.getFcmtoken(), user);


				        
			        	return UpdateProfile(patientModel, user);
				        

			        }
			        catch(AuthenticationException autherror)
			        {
						throw new ConflictException(String.format("You already Exist in System with phone number: [%s] ;  Please Enter the CORRECT provided Passowrd ", user.getMobile()));

			        }
			        catch (Exception e) {
						throw new ConflictException(String.format("You already Signed Up in System with phone number: [%s] ;  Please Login instead", user.getMobile()));
			        }
					
				
			}
			
			
			user = new User(patientModel.getName(), 
							userService.passwordEncoder(patientModel.getPassword())
							, patient.USER_TYPE, 
							patientModel.getMobile(), 
							patientModel.getFcmtoken(),userService.USER_DEFAULT_PROFILE_IMAGE);
			
			
			//Save User		
			User usr = userService.addUser(user);
					
			patient patient = new patient( usr.getId(), patientModel.getAge(), patientModel.getAddress());
					
			patient.setUser(usr);
					
			return  patientRepository.save(patient);

		}
		  

		
	
		
	
		
		public patient UpdateProfile(PatientDTO patientModel,User user)
		{
			
			try {
				patient patient = patientRepository.findById(user.getId()).get();

				//Update variables
				patient.getUser().setName(patientModel.getName());
				patient.setAge(patientModel.getAge());
				patient.setAddress(patientModel.getAddress());
				
				patient.setUser(userService.addUser(patient.getUser()));
				return patientRepository.save(patient);
	
	
				
			}
			catch (Exception e) 
			{
				throw new ConflictException(String.format("Problem with Update Profile:  [%s] ", e.getMessage().toString()));
			}
		
		}
		
		
		public User UpdatefcmToken(String fcmtoken,User user)
		{
			return userService.UpdatefcmToken(fcmtoken, user);
		
		}
		
		public void sendNotification(data data, String FCMToken) 
		{	
			notification notification = new notification(data.getTitle(), data.getBody());

			try {
				PersonalMessageSender.callToFcmServer(notification, data, FCMToken);
			}
			catch (Exception e) {
				throw new ConflictException(String.format("Problem with sending Notification"));
			}
		}
     
	
}

package root.user.patient;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;



public class PatientDTO{
	
	
	
	private int id;
	
	@NotEmpty(message = "name can't be Empty")
	private String name;	
	
	@Size(min=4,message="Minimum password Size > 8")
	private String password;
	
	private int age;	
	

	private String fcmtoken;
	
	
	private String address; //SEARCH FOR IF GOVEN OR NOT 
	private Date timestamp;	
	private String userType;

	private String userImage;
	
	
	private String mobile;

	
	public PatientDTO() {
	
	}
	//CREATION
	public PatientDTO(String name, String password, int age , String address , String fcmtoken,  String mobile )
	{

		this(name, password, age, fcmtoken, mobile);
		this.setAddress(address);
	}
	
	public PatientDTO(String name,  String password, int age,  String fcmtoken,  String mobile )
	{
		this();
		this.setAge(age);
		this.setName(name);
		this.setPassword(password);
		this.setFcmtoken(fcmtoken);
		this.setMobile(mobile);

	}
	public PatientDTO(String name, int age,String address  )
	{
		this.setAge(age);
		this.setName(name);
		this.setAddress(address);

	}
	
	public PatientDTO(String mobile, String password)
	{
		this.setMobile(mobile);
		this.setPassword(password);

	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public int getUserId() {
		return id;
	}

	public void setUserId(int id) {
		this.id = id;
	}

	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getFcmtoken() {
		return fcmtoken;
	}
	public void setFcmtoken(String fcmtoken) {
		this.fcmtoken = fcmtoken;
	}
		

}

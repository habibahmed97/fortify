package root.user.patient;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import root.user.patient.patient;

@Repository
public interface PatientRepository extends  JpaRepository<patient, Integer>{

	
	@Query(value="SELECT us.id, us.name, us.fcmtoken, us.mobile, doc.years_experience, relation.relationid, us.user_image, doc.session_id FROM patient_doctor_relation relation INNER JOIN doctor doc ON relation.doctorid = doc.id INNER JOIN user us ON  us.id = doc.id	Where relation.patientid = ?1", nativeQuery=true)
	List<Object> getDoctorList(int patientid);
	
	@Transactional
    @Modifying(clearAutomatically = true)
	@Query(value="Insert into vitals (id,weight,height,BMI, body_fats_ratio, body_water_ratio, stomic_area_fats , bone_desity, muscle_desity ) values (:patientid,0,0,0,0,0,0, 0, 0)", nativeQuery=true)
	void addNewestVitals(@Param("patientid") int patientid);
}


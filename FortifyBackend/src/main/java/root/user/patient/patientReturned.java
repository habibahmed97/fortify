package root.user.patient;

import java.util.List;


public class patientReturned {

	
	
	private patient myPatient;
	private List<Object> doctorsArrayList;
	
	
	public patientReturned(patient myPatient, List<Object> list) {
		super();
		this.myPatient = myPatient;
		this.setDoctorsArrayList(list);
	}
	
	public patient getMyPatient() {
		return myPatient;
	}
	public void setMyPatient(patient myPatient) {
		this.myPatient = myPatient;
	}

	public List<Object> getDoctorsArrayList() {
		return doctorsArrayList;
	}

	public void setDoctorsArrayList(List<Object> doctorsArrayList) {
		this.doctorsArrayList = doctorsArrayList;
	}
	

}


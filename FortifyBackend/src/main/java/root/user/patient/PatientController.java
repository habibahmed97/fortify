package root.user.patient;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import root.security.BaseController;
import root.user.User;
import root.user.patient.PatientDTO;
import root.user.patient.patient;
import root.user.patientDoctorRelation.GetRelationService;
import  root.user.patient.PatientService;

@RestController
public class PatientController extends BaseController {

	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private GetRelationService relationService;
	
	

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET,value ="/DoctorPatient/getMyDoctorList")
	public ResponseEntity<List<Object>> getMyDoctors() 
	{
		return new ResponseEntity<>(relationService.getDoctorz(getCurrentUser().getId()),HttpStatus.OK);
	}
	
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/patient/api/createPatient" ,consumes = "application/json") //public
	public ResponseEntity<patient> createPatient(@Valid @RequestBody PatientDTO patientModel)
	{
	  return new ResponseEntity<>(patientService.createPatient(patientModel), HttpStatus.CREATED); 
	}

	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/patient/updateProfile" ) 
	public ResponseEntity<patient> updateMyProfile(@Valid @RequestBody PatientDTO patientModel)
	{
		  return new ResponseEntity<>(patientService.UpdateProfile(patientModel, getCurrentUser()), HttpStatus.OK); 
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/patient/updateToken" ,consumes = "application/json") 
	public ResponseEntity<User> updateMyFCMToken(@Valid @RequestBody Map<String, String> body)
	{
		return new ResponseEntity<>(patientService.UpdatefcmToken(body.get("fcmtoken").toString(),getCurrentUser()), HttpStatus.OK); 
 
	}



	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, value = "/patient/getCurrentPatientData")
	public ResponseEntity<Object> getCurrentPatient() 
	{	
		return new ResponseEntity<>(patientService.getPatientByUserId(getCurrentUser().getId()),HttpStatus.OK);
	}
	



	

	
}

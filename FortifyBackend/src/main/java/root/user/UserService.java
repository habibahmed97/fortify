package root.user;


import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import root.error.ConflictException;
import root.error.NotFoundException;
import root.s3.AmazonClient;
import root.s3.url;
import root.user.User;
import root.user.UserRepository;




@Service
public class UserService implements UserDetailsService{

		public final String USER_DEFAULT_PROFILE_IMAGE="https://fortifyfitness.s3.ap-south-1.amazonaws.com/avatar.jpg";

		@Autowired
		  private AmazonClient amazonClient;
		
		@Autowired
		private UserRepository userRepository;
		
		@Bean
		private PasswordEncoder passwordEncoder()
		{
			return new BCryptPasswordEncoder();
		}

		@Override
		public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
			User user = userRepository.findByMobile(username);
			
			if(user!=null)
			{
				return user;
			}
			
			throw new NotFoundException(String.format("No Record of the email [%s] was found in our database",username ));
		}

		
		public User SendIfUserExist(String mobile) {
			
			User myUser = userRepository.findByMobile(mobile);
			
			if(myUser!=null)
			{
				return myUser;
			}
			
			return null;
			
		}


		public User addUser(User user)
		{
			return userRepository.save(user);
		}
		
		
		public User getUserbyId(int id)
		{
			 try 
			 {
			   return userRepository.findById(id).get();
		     }
		     catch(NoSuchElementException ex)
		     {
		       throw new NotFoundException(String.format("No USER Record with the id [%s] was found in our database", id));
		     }
			
		}
		
		public url updateImage(MultipartFile file,User user)
		{		
			
			try {
				
			String urlString = this.amazonClient.uploadFile(file);
			String OldImageURL= user.getUser_image();
			
			user.setUser_image(urlString);
			this.addUser(user);
			
			this.amazonClient.deleteFileFromS3Bucket(OldImageURL);
			return new url(urlString);

			}
			catch (Exception e) {
				 throw new ConflictException(String.format("Error in updating profile image  :[%s] ", e.getMessage().toString()));
			}

		}
		
		public String passwordEncoder(String needEncoding)
		{
			return passwordEncoder().encode(needEncoding);
		}
		public User UpdatefcmToken(String fcmtoken,User user)
		{
			try {
				user.setFcmToken(fcmtoken);
				return this.addUser(user);
			}
			catch (Exception e) 
			{
				throw new ConflictException(String.format("Problem with Update Profile; Try again Later"));
			}
		
		}
	

}

package root.user.patientDoctorRelation;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import root.user.patientDoctorRelation.PatienDoctorRelation;



@Repository
public interface PatientDoctorRelationRepository extends CrudRepository<PatienDoctorRelation, Integer>{

//	SELECT COUNT(1)
//	FROM patient_doctor_relation
//	WHERE doctorid = 25 and patientid=56;
//	
	@Query(value = "SELECT COUNT(1) FROM patient_doctor_relation relation WHERE relation.doctorid = ?1 and relation.patientid= ?2", nativeQuery=true)
	int getCountOfExitingRelation(int doctorid, int patientid);

}


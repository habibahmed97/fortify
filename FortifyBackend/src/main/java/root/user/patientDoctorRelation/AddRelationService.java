package root.user.patientDoctorRelation;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.error.ConflictException;
import root.notify.PersonalMessageSender;
import root.notify.SMS;
import root.notify.data;
import root.notify.notification;
import root.user.User;
import root.user.UserService;
import root.user.patient.PatientRepository;
import root.user.patient.patient;



@Service
public class AddRelationService {


	
	@Autowired
	private PatientDoctorRelationRepository relationRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PatientRepository patientRepo;
	
	

	//When Doctor
	public User AddPatient(String mobile, int doctorid, String doctorName){
		
	
			User user = userService.SendIfUserExist(mobile);

			if(user!=null) //Exist
			{
				if(DoesRelationExistBeforeHand(doctorid, user.getId()))
				{
				       throw new ConflictException(String.format("This patient [%s] is already in your list", user.getName()));
				}
				else {
					data data = new data("A Doctor Has Added You", "You have become one of Dr."+ doctorName + " patients");
					sendNotification(data, user.getFcmToken());
				}
			}
			else {  //NOT Exist
				
				user = CreateEmptyPatientinDatabase(mobile);

			}
			
			this.addRelation(user.getId(),doctorid);
			return user;
	}
	
	public boolean DoesRelationExistBeforeHand(int doctorid, int patientid)
	{
		if(this.relationRepository.getCountOfExitingRelation(doctorid, patientid)>0)
		{
			System.out.println("relation exist: "+ this.relationRepository.getCountOfExitingRelation(doctorid, patientid) );
			return true;
		}
		System.out.println("relation don't exist: "+ this.relationRepository.getCountOfExitingRelation(doctorid, patientid));
		return false;
	}
	
	public User CreateEmptyPatientinDatabase(String mobile){
		
		String password = createRandomPassword();
		User user2 = new User(mobile,userService.passwordEncoder(password));
		User user = userService.addUser(user2);
		patientRepo.save(new patient(user));
		messageThePatientNumber(mobile,password);
		return user;

	}
	
	public String createRandomPassword() {
		return RandomStringUtils.random(4, false, true); 
	}
	
	
	
	public void addRelation(int patientId, int doctorId)
	{
		relationRepository.save(new PatienDoctorRelation(doctorId,patientId));
	
	}
	
	public void sendNotification(data data, String FCMToken) 
	{	
		notification notification = new notification(data.getTitle(), data.getBody());

		try {
			PersonalMessageSender.callToFcmServer(notification, data, FCMToken);
		}
		catch (Exception e) {
			throw new ConflictException(String.format("Problem with sending Notification"));
		}
	}


	public void messageThePatientNumber(String mobile, String passCode) {
		SMS.smsSend(mobile,passCode);
	}
	
}

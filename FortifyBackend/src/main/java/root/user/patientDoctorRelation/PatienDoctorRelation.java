package root.user.patientDoctorRelation;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Table(name = "patient_doctor_relation") //This is required if your table name is different from the class name.
@Entity 
public class PatienDoctorRelation{

			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)// so it won't generate Id itself
			private Integer relationid;

			@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
			private Date timestamp;			

			@Column(name="patientid")
			private int patientid;
			
			@Column(name="doctorid")
			private int doctorid;
			

		public PatienDoctorRelation() {
			 setTimestamp();
		}
		 
		 public PatienDoctorRelation(int doctorid, int patientid) {
			 this();
			 setDoctorid(doctorid);
			 setPatientid(patientid);
			}

		public Integer getRelationid() {
			return relationid;
		}

		public void setRelationid(Integer relationid) {
			this.relationid = relationid;
		}

		public int getDoctorid() {
			return doctorid;
		}

		public void setDoctorid(int doctorid) {
			this.doctorid = doctorid;
		}

		public int getPatientid() {
			return patientid;
		}

		public void setPatientid(int patientid) {
			this.patientid = patientid;
		}

		public Date getTimestamp() {
			return timestamp;
		}

		public void setTimestamp() {
			this.timestamp = new Date();
		}
				
			
	
			
			
			
			


}

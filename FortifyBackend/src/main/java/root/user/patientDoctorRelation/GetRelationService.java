package root.user.patientDoctorRelation;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.user.doctor.DoctorRepository;
import root.user.patient.PatientRepository;


@Service
public class GetRelationService {
	
	@Autowired
	private PatientRepository patientRepository;
	

	@Autowired
	private DoctorRepository docRepo;
	
	public List<Object> getDoctorz(int patientid) 
	{
		return this.patientRepository.getDoctorList(patientid);
	}
	
	
	public List<Object> getPatients(int doctorId) 
	{
		
		return this.docRepo.getPatientList(doctorId);
	}

}



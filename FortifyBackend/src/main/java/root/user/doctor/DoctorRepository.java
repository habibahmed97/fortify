package root.user.doctor;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import root.user.doctor.doctor;


@Repository
public interface DoctorRepository extends CrudRepository<doctor, Integer>{
	
	@Query(value="SELECT us.id, us.name, us.fcmtoken, us.mobile, pat.age, pat.address, rel.relationid, us.user_image FROM patient_doctor_relation rel INNER JOIN patient pat  ON rel.patientid = pat.id INNER JOIN user us ON  us.id = pat.id Where rel.doctorid = ?1", nativeQuery = true)
	ArrayList<Object> getPatientList(int doctorid);

}


package root.user.doctor;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.amazonaws.services.appstream.model.Session;



public class DoctorDTO{

	private Integer doctor_id;
	private int userId;	
	@NotEmpty(message = "name can't be Empty")
	private String name;
	
	private String sessionId;
	
	@Size(min=4,message="Minimum password Size > 8")
	private String password;
	
	private int years_experience;
	private Date timestamp;	
	private String userType;
	private String fcmtoken;
	private String mobile;

	public DoctorDTO() {
		// TODO Auto-generated constructor stub
	}

	//CREATION
	public DoctorDTO(Integer doctor_id,int userId,String name, String password, int years_experience ,
			String mobile, String fcmtoken, String sessionId)
	{

		this(name, password, years_experience, fcmtoken, mobile,sessionId );
		this.setTimestamp(timestamp);
		this.setDoctor_id(doctor_id);
		this.setUserId(userId);
		
		
		
	}
	public DoctorDTO(String name ,int years_experience)
	{

		this.setName(name);
		this.setYears_experience(years_experience);
		
	}
	
	public DoctorDTO(String name, String password, int years_experience ,  
			String fcmtoken,  String mobile, String sessionId )
	{
	
		this.setFcmtoken(fcmtoken);
		this.setMobile(mobile);
		this.setYears_experience(years_experience);
		this.setName(name);
		this.setPassword(password);
		this.setSessionId(sessionId);
	

	}
	
	
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}


	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Integer getDoctor_id() {
		return doctor_id;
	}
	public void setDoctor_id(Integer doctor_id) {
		this.doctor_id = doctor_id;
	}
	public int getYears_experience() {
		return years_experience;
	}
	public void setYears_experience(int years_experience) {
		this.years_experience = years_experience;
	}

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFcmtoken() {
		return fcmtoken;
	}

	public void setFcmtoken(String fcmtoken) {
		this.fcmtoken = fcmtoken;
	}
		

}

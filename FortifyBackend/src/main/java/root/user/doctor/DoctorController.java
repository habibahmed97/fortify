package root.user.doctor;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import root.security.BaseController;
import root.user.User;
import root.user.doctor.DoctorDTO;
import root.user.doctor.doctor;
import root.user.patientDoctorRelation.AddRelationService;
import root.user.patientDoctorRelation.GetRelationService;
import root.user.doctor.DoctorService;


@RestController
public class DoctorController extends BaseController {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private AddRelationService addrelationService;
  
	@Autowired
	private GetRelationService getrelationService;
  

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/addPatient")
	public ResponseEntity<User> addPatientUsingPhoneNumber(@RequestBody Map<String,Object> body) 
	{
		return new ResponseEntity<>(addrelationService.AddPatient(body.get("mobile").toString(),getCurrentUser().getId() , getCurrentUser().getName()),HttpStatus.OK);
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/doctor/api/createDoctor") //public
	public ResponseEntity<doctor> createdoctor(@Valid @RequestBody DoctorDTO doctorModel)
	{
	  return new ResponseEntity<>(doctorService.createDoctor(doctorModel), HttpStatus.CREATED); 
	}
	    

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET,value ="/doctor/getCurrentDoctorData")
	public ResponseEntity<doctorReturned> getCurrentdoctor() 
	{
		
		return new ResponseEntity<>(doctorService.getDoctorByUserId(getCurrentUser().getId()),HttpStatus.OK);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/doctor/updateProfile" ,consumes = "application/json") 
	public ResponseEntity<doctor> updateProfile(@Valid @RequestBody DoctorDTO doctorModel)
	{
		
	  return new ResponseEntity<>(doctorService.UpdateProfile(doctorModel, getCurrentUser()), HttpStatus.OK); 
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/doctor/updateToken" ,consumes = "application/json") 
	public ResponseEntity<User> updateToken(@Valid @RequestBody Map<String, String> body)
	{
		
	  return new ResponseEntity<>(doctorService.UpdatefcmToken(body.get("fcmtoken").toString(),  getCurrentUser()), HttpStatus.OK); 
	}

	

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET,value ="/DoctorPatient/getMyPatientList")
	public ResponseEntity<List<Object>> getPatientz() 
	{
		return new ResponseEntity<>(getrelationService.getPatients(getCurrentUser().getId()),HttpStatus.OK);
	}


	

	
}

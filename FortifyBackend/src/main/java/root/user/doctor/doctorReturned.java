package root.user.doctor;

import java.util.List;


public class doctorReturned {

	
	
	private doctor mydoctor;
	private List<Object> patientArrayList;
	
	
	public doctorReturned(doctor mydoctor, List<Object> list) {
		super();
		this.mydoctor = mydoctor;
		this.setPatientArrayList(list);
	}
	
	public doctor getMydoctor() {
		return mydoctor;
	}
	public void setMydoctor(doctor mydoctor) {
		this.mydoctor = mydoctor;
	}

	public List<Object> getPatientArrayList() {
		return patientArrayList;
	}

	public void setPatientArrayList(List<Object> patientArrayList) {
		this.patientArrayList = patientArrayList;
	}


	

}


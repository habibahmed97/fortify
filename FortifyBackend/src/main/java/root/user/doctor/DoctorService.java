package root.user.doctor;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import root.error.ConflictException;
import root.user.User;
import root.user.UserService;


	
@Service
public class DoctorService {
	
		@Autowired
		private UserService userService;

		@Autowired
		private DoctorRepository doctorRepository;

		
		public doctorReturned getDoctorByUserId(int id)
		{
		
			doctor doctor = doctorRepository.findById(id).get();
			if(doctor==null) 
			{
				throw new ConflictException(String.format("You are not a Doctor Record with Id [%s] in our database", id));
			}

			return new doctorReturned(doctor,doctorRepository.getPatientList(id) );

	    }
			
	
		
		
		public doctor createDoctor(DoctorDTO doctorModel)
		{	
			User userExist = userService.SendIfUserExist(doctorModel.getMobile());
			if(userExist!=null) //Exist
			{
				throw new ConflictException(String.format("You already Exist in System with phone number: [%s] ;  Please Login", userExist.getMobile()));
			}
			
				
				User user = new User(doctorModel.getName() 
						,userService.passwordEncoder(doctorModel.getPassword())
						, doctor.USER_TYPE, doctorModel.getMobile(), doctorModel.getFcmtoken(),
						userService.USER_DEFAULT_PROFILE_IMAGE);
				User usr = 	userService.addUser(user);	
				doctor doctor = new doctor(usr.getId(), doctorModel.getYears_experience(),doctorModel.getSessionId());

				doctor.setUser(usr);
	            return doctorRepository.save(doctor);

	
			
		}
		
		public doctor UpdateProfile(DoctorDTO doctorModel,User user)
		{
			    try {
				//get My doctor Data
				doctor doc = doctorRepository.findById(user.getId()).get();
				
				
				//Update variables
				user.setName(doctorModel.getName());
				doc.setYears_experience(doctorModel.getYears_experience());
					
				doc.setUser(userService.addUser(user));
				return doctorRepository.save(doc);
	
			}
			catch (Exception e) 
			{
				throw new ConflictException(String.format("Problem with Update Profile; Try again Later"));
			}
		
		}
		
		
		//Fordevelopers
		public User UpdatefcmToken(String fcmtoken,User user)
		{
			return userService.UpdatefcmToken(fcmtoken, user);
		
		}

		


}

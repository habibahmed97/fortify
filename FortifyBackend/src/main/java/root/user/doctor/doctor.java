package root.user.doctor;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import root.user.User;


@Table(name = "doctor") //This is required if your table name is different from the class name.
@Entity 
public class doctor  implements Serializable{

	private static final long serialVersionUID = 1L;

	public static final String USER_TYPE ="doctor";
	
			//@GeneratedValue(strategy = GenerationType.AUTO)
			@Id
			private Integer id;


			@OneToOne(cascade=CascadeType.ALL)
		    @JoinColumn(name = "id")
			private User user;

			@Column(name="years_experience")
			private int years_experience;

			@Column(name="sessionId")
			private String sessionId;

//
//			@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//			@JoinColumn(name="doctorid")
//		    private List<PatienDoctorRelation> relationToPatient;
	
			
			public doctor() {
			}
			public doctor( int id ,int years_experience, String sessionId)
			{
				this.setId(id);
				this.setYears_experience(years_experience);
				this.setSessionId(sessionId);
			}
			
					
//			public List<PatienDoctorRelation> getRelationToPatient() {
//				return relationToPatient;
//			}
//	
//			public void add(PatienDoctorRelation relation)
//			{
//				if(relationToPatient==null)
//				{
//					relationToPatient = new ArrayList<PatienDoctorRelation>();
//				}
//				relationToPatient.add(relation);
//				
//			}
			
			public Integer getId() {
				return id;
			}
			public void setId(Integer id) {
				this.id = id;
			}
			public User getUser() {
				return user;
			}

			public void setUser(User user) {
				this.user = user;
			}

			public int getYears_experience() {
				return years_experience;
			}

			public void setYears_experience(int years_experience) {
				this.years_experience = years_experience;
			}
			public String getSessionId() {
				return sessionId;
			}
			public void setSessionId(String sessionId) {
				this.sessionId = sessionId;
			}
			
			


}

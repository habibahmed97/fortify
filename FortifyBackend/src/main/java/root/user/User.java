package root.user;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonFormat;

import root.user.patient.patient;

@Table(name = "user")
@Entity 
public class User implements UserDetails{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	 
	@NotEmpty(message = "name can't be Empty")
	private String name;
	
	@NotEmpty(message = "mobile number can't be Empty")
	private String mobile;
	
	@NotEmpty(message = "must instantiate the mobile token of user")
	private String fcmtoken;
	
	@Size(min=4,message="Minimum password Size > 8")
	@NotNull(message = "password can't be null")
	private String password;
	
	@NotEmpty(message = "must instantiate the type of user")
	private String type;
	  
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private Date timestamp;
	
	private String user_image;
	
	public User(String mobile, String password)
	{
		this.setTimestamp();
		this.setPassword(password);
		this.setName("Change Your Data");
		this.setType(patient.USER_TYPE);
		this.setFcmToken("NOT YET DEFINED");
		this.setMobile(mobile);
	}
	
	public User() {
		
	}
	public User(int id) 
	{
	   this.setTimestamp();
	   this.setId(id);
	    
	}
	
	public User(String name, String password, String type, String mobile, String fcmToken)
	{
		this.setTimestamp();
		this.setName(name);
		this.setPassword(password);
		this.setType(type);
		this.setFcmToken(fcmToken);
		this.setMobile(mobile);
		
	}
	
	public User(String name, String password, String type, String mobile, String fcmToken, String userImage)
	{
		this.setTimestamp();
		this.setName(name);
		this.setPassword(password);
		this.setType(type);
		this.setFcmToken(fcmToken);
		this.setMobile(mobile);
		this.setUser_image(userImage);
		
	}
	


	public String getUser_image() {
		return user_image;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

	public void setAll(String name, String password, String type, String fcmToken)
	{
		this.setTimestamp();
		this.setName(name);
		this.setPassword(password);
		this.setType(type);
		this.setFcmToken(fcmToken);
		this.setMobile(mobile);
		
	}

	

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getFcmToken() {
		return fcmtoken;
	}
	public void setFcmToken(String fcmToken) {
		this.fcmtoken = fcmToken;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp() {
		this.timestamp = new Date();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getUsername() {
		return getMobile();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
}

package root.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import root.s3.url;
import root.security.BaseController;

@RestController
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST,value ="/user/uploadImage" ) 
	public url uploadImage(@RequestPart(value = "file") MultipartFile file)
	{
	  return this.userService.updateImage(file, getCurrentUser()); 
	}
}



	

